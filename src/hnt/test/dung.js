/**
 * Note object
 */
function Note(content) {
	this.content = content;
	this.date = new Date();
	this.toHTML = toHTML;

	function toHTML() {
		return this.date + "<br/>" + this.content + "<br />++++++++++++++++++++++++---<br/>"
	}
}

/**
 * Note controller used for display, add, edit, delete note.
 */
function NotesController() {
	this.list = [];
	this.displayNotes = displayNotes;
	this.addNote = addNote;
	this.editNote = editNote;
	this.deleteNode = deleteNode;

	function displayNotes(id) {
		var component = document.getElementById(id);
		component.innerHTML = "";
		for (var count = 0; count < this.list.length; count++) {
			component.innerHTML += this.list[count].toHTML();
		}
	}

	function addNote(content, callback) {
		this.list.push(new Note(content));
		callback();
	}
	
	function deleteNode(date){
		//TODO: implement later
		return true;
	}
	
	function editNote(date){
		//TODO: implement later
		return true;
	}
}

function ViewController() {
	this.goToViewPage = goToViewPage;

	function goToViewPage(callback) {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("demo").innerHTML = this.responseText;
				callback();
			}
		};
		xhttp.open("GET", "dunglist.html", true);
		xhttp.send();
	}
	
	function goToAddPage(callback) {
		var xhttp = new XMLHttpRequest();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				document.getElementById("demo").innerHTML = this.responseText;
				callback();
			}
		};
		xhttp.open("GET", "dungadd.html", true);
		xhttp.send();
	}
}
